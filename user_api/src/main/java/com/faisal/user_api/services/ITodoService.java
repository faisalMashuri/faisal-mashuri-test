package com.faisal.user_api.services;

import com.faisal.user_api.model.TodoRequest;

public interface ITodoService {
    void CreateTodo(TodoRequest request, String token);
    void UpdateTodo(String token, String todo_id, TodoRequest request);
    void DeleteTodo(String token, String todo_id);
}

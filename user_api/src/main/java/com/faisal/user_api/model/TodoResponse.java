package com.faisal.user_api.model;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TodoResponse {
    private String id;
    private String title;
    private Boolean isDone;

    public TodoResponse(String id, String title,Boolean isDone ) {
        this.id = id;
        this.title = title;
        this.isDone = isDone;
    }
}

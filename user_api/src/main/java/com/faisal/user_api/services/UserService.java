package com.faisal.user_api.services;

import com.faisal.user_api.config.JWTConfig;
import com.faisal.user_api.entity.User;
import com.faisal.user_api.model.*;
import com.faisal.user_api.repositories.UserRepository;
import com.faisal.user_api.security.BCrypt;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;


@Service
public class UserService implements IUserService{
    @Autowired
    private UserRepository userRepository;
    private JWTService jwtService;
    private RedisService redisService;

    private Validator validator;

    public UserService(UserRepository userRepository, Validator validator, JWTService jwtService, RedisService redisService) {
        this.userRepository = userRepository;
        this.validator = validator;
        this.jwtService = jwtService;
        this.redisService = redisService;
    }

    @Transactional(rollbackFor = Exception.class)
    public void register(RegisterUserRequest request){
        Set<ConstraintViolation<RegisterUserRequest>> constraintViolations = validator.validate(request);
        if (!constraintViolations.isEmpty()) {
            throw new ConstraintViolationException(constraintViolations);
        }

        if (userRepository.existsByUsername(request.getUsername())) {
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST,"username already exists");
        }

        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setUsername(request.getUsername());
        user.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
        user.setName(request.getName());
        userRepository.save(user);
    }

    @Transactional(rollbackFor = Exception.class)
    public TokenResponse login(LoginRequest request) {
        Set<ConstraintViolation<LoginRequest>> constraintViolations = validator.validate(request);
        if (!constraintViolations.isEmpty()) {
            throw new ConstraintViolationException(constraintViolations);
        }
        User user = userRepository.findByUsername(request.getUsername());
        if (user == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "wrong username");
        }

        if (BCrypt.checkpw(request.getPassword(), user.getPassword())) {
           String token = jwtService.GenerateToken(user);
           userRepository.save(user);

           return TokenResponse
                   .builder()
                   .token(token)
                   .expiredAt(expired30days())
                   .build();
        }else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "wrong password");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public Response getAllTodoByUser(String token) {
        Response responseType = new Response();
        List<TodoResponse> todoResponses = new ArrayList<>();
        String username = jwtService.extractUsername(token);
        User userData = userRepository.findByUsername(username);
        if (userData == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "credential not found");
        }

        Response redisData = redisService.getValue(userData.getId() + "todo",Response.class);
        if (redisData != null ) {
            return redisData;
        }

        List<Object[]> data = userRepository.getAllTodoByUser(userData.getId());
        data.forEach(row -> {
            TodoResponse todoResponse = new TodoResponse(row[2].toString(), row[3].toString(),Boolean.valueOf(row[4].toString()));
            todoResponses.add(todoResponse);
        });
        Response resp = new Response(userData.getId(), userData.getName(), todoResponses);
        redisService.setValue(userData.getId() + "todo",resp);
        return resp;
    }


    private Long expired30days() {
        return System.currentTimeMillis() + (1000 * 16 * 24 *30);
    }

}

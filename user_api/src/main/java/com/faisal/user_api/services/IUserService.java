package com.faisal.user_api.services;

import com.faisal.user_api.model.LoginRequest;
import com.faisal.user_api.model.RegisterUserRequest;
import com.faisal.user_api.model.Response;
import com.faisal.user_api.model.TokenResponse;

public interface IUserService {
    void register(RegisterUserRequest request);
    TokenResponse login(LoginRequest request);
    Response getAllTodoByUser(String token);
}

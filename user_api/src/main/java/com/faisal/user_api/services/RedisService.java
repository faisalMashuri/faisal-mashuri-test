package com.faisal.user_api.services;

import com.faisal.user_api.model.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisService {
    private final StringRedisTemplate redisTemplate;

    @Autowired
    public RedisService(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void setValue(String key, Object value) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonValue = mapper.writeValueAsString(value);
            redisTemplate.opsForValue().set(key, jsonValue);
        } catch (JsonProcessingException e) {
            // Handle JSON processing exception
            throw new RuntimeException(e);
        }
    }

    public <T> T getValue(String key, Class<T> valueType) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonValue = redisTemplate.opsForValue().get(key);
        if (jsonValue != null) {
            try {
                return mapper.readValue(jsonValue, valueType);
            } catch (JsonProcessingException e) {
                // Handle JSON processing exception
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    public void removeValue(String key) {
        redisTemplate.delete(key);
    }


}

package com.faisal.user_api.services;

import com.faisal.user_api.config.JWTConfig;
import com.faisal.user_api.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.*;
import java.util.function.Function;

@Component
public class JWTService {
    @Autowired
    private JWTConfig jwtConfig;

    public JWTService(JWTConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }
    public Claims verifyToken(String token) {
        try {
            // Parse the token and verify it using the secret key
            byte[] secretByte = Decoders.BASE64.decode(jwtConfig.getSecret());
            Key key = Keys.hmacShaKeyFor(secretByte);
            Claims claims = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody();

            // Check if the token has expired
            if (claims.getExpiration().before(new Date())) {
                throw new Exception("Token has expired");
            }

            return claims; // Return the claims if token is valid
        } catch (Exception e) {
            e.printStackTrace();
            return null; // Return null if token is invalid
        }
    }

    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    public Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSignKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public Boolean validateToken(String token, User userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }


    public String GenerateToken(User user){
        Map<String, Object> claims = new HashMap<>();
        claims.put("username" , user.getUsername());
        claims.put("name", user.getName());
        claims.put("id", user.getId());
        return createToken(claims, user);
    }

    private String createToken(Map<String, Object> claims, User user) {

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(user.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(expired30days())
                .signWith(getSignKey(), SignatureAlgorithm.HS256).compact();
    }

    private Key getSignKey() {
        byte[] keyBytes = Decoders.BASE64.decode(jwtConfig.getSecret());
        return Keys.hmacShaKeyFor(keyBytes);
    }

    private Date expired30days() {
        Calendar calendar = Calendar.getInstance();
        // Menambahkan 30 hari ke waktu saat ini
        calendar.add(Calendar.DAY_OF_MONTH, 30);
        // Mendapatkan tanggal 30 hari dari sekarang
        return calendar.getTime();
    }

}

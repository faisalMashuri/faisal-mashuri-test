package com.faisal.user_api.repositories;

import com.faisal.user_api.entity.User;
import com.faisal.user_api.model.RegisterUserRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String>{
    Boolean existsByUsername(String username);
    User findByUsername(String username);

    @Query(value = "SELECT u.id, u.name, t.id, t.title, t.done FROM todos t JOIN users u ON t.user_id = :user_id WHERE t.deleted_at IS NULL ORDER BY t.created_at", nativeQuery = true)
    List<Object[]> getAllTodoByUser(String user_id);

}

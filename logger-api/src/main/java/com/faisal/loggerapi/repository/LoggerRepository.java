package com.faisal.loggerapi.repository;

import com.faisal.loggerapi.Message;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LoggerRepository extends MongoRepository<Message, String> {
}

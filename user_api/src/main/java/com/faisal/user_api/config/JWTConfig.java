package com.faisal.user_api.config;

import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JWTConfig {
    @Autowired
    private Dotenv dotEnv;

    public String getSecret() {
        return dotEnv.get("SECRET");
    }
}

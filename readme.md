## Mini Project ( Todo application )
Feature :
1. Microservice & Containerization
   1. user-api - feature
      1. database : mysql
      2. cache : redis
      3. kafka producer
         ![Kafka producer](kafka%20producer.png "Title")
      4. validation
      5. Authentication & Authorization JWT
      6. Spring Ioc
      7. Java Stream
      8. Advance Native query SQL
   2. logger-api - feature
      1. database - mongoDB
      2. kafka consumer
         ![Kafka producer](kafka%20consumer.png "Title")
      

## How to run
1. Clone projectnyta
2. jalankan `docker-compose up`
3. lalu buka terminal lagi, masuk ke folder user-api
dan jalankan `mvn spring-boot:run`
4. lalu buka terminal lagi, masuk ke folder logger-api
dan jalankan `mvn spring-boot:run`

## How to test
1. download collection
2. import ke dalam postman
3. lalu silahkan testing
package com.faisal.user_api.controller;

import com.faisal.user_api.config.JWTConfig;
import com.faisal.user_api.model.*;
import com.faisal.user_api.services.UserService;
import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.awt.*;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/api/users/todo")
    public WebResponse<Response> getAllTodoByUser(@RequestHeader("Authorization") String authorizationHeader) {
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            throw  new ResponseStatusException(HttpStatus.UNAUTHORIZED,"JWT token is required");
        }
        String token = authorizationHeader.substring(7);
        Response data = userService.getAllTodoByUser(token);
        return WebResponse.<Response>builder().data(data).build();
    }


    @PostMapping(
            path = "/api/auth/register",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<String> register(@RequestBody RegisterUserRequest request) {
        userService.register(request);
        return WebResponse.<String>builder().data("Ok").build();
    }

    @PostMapping(
            path = "/api/auth/login",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<TokenResponse> login(@RequestBody LoginRequest request) {
        TokenResponse tokenResponse = userService.login(request);
        return WebResponse.<TokenResponse>builder().data(tokenResponse).build();
    }





}

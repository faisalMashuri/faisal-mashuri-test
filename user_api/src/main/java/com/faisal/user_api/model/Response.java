package com.faisal.user_api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
public class Response {
    private String name;
    private String id;
    private List<TodoResponse> todo;

    public Response(String name,String id, List<TodoResponse> todo) {
        this.name = name;
        this.id = id;
        this.todo = todo;
    }
}



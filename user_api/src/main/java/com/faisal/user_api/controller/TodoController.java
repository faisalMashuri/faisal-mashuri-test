package com.faisal.user_api.controller;

import com.faisal.user_api.messaging.KafkaProducer;
import com.faisal.user_api.model.MessageData;
import com.faisal.user_api.model.TodoRequest;
import com.faisal.user_api.model.WebResponse;
import com.faisal.user_api.services.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;

@RestController
public class TodoController {
    @Autowired
    private TodoService todoService;
    private final KafkaProducer kafkaProducer;

    public TodoController(KafkaProducer kafkaProducer) {
        this.kafkaProducer = kafkaProducer;
    }

    @GetMapping(path = "/api/todos")
    public String todoStatus() {
        return "todo status running";
    }


    @PostMapping(
            path = "/api/users/todos",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<String> CreateTodo(@RequestHeader("Authorization") String authorizationHeader, @RequestBody TodoRequest request) {
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            throw  new ResponseStatusException(HttpStatus.UNAUTHORIZED,"JWT token is required");
        }
        String token = authorizationHeader.substring(7);
        todoService.CreateTodo(request, token);
        MessageData messageData = new MessageData();
        messageData.setAction("create todo");
        messageData.setKey(String.valueOf(new Date()));
        messageData.setData(request);
        kafkaProducer.sendMessage(messageData);
        return WebResponse.<String>builder().data("OK").build();
    }

    @PutMapping(
            path = "/api/users/todos/{todoId}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<String> UpdateTodo(
                @RequestHeader("Authorization") String authorizationHeader,
                @PathVariable("todoId") String todoId,
                @RequestBody TodoRequest request
            ) {
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            throw  new ResponseStatusException(HttpStatus.UNAUTHORIZED,"JWT token is required");
        }
        String token = authorizationHeader.substring(7);
        todoService.UpdateTodo(token, todoId, request);
        MessageData messageData = new MessageData();
        messageData.setAction("update todo");
        messageData.setKey(String.valueOf(new Date()));
        messageData.setData(request);
        kafkaProducer.sendMessage(messageData);
        return WebResponse.<String>builder().data("Ok").build();
    }
    @DeleteMapping(
            path = "/api/users/todos/{todoId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public WebResponse<String> DeleteTodo(
            @RequestHeader("Authorization") String authorizationHeader,
            @PathVariable("todoId") String todoId
    ) {
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            throw  new ResponseStatusException(HttpStatus.UNAUTHORIZED,"JWT token is required");
        }
        String token = authorizationHeader.substring(7);
        todoService.DeleteTodo(token, todoId);
        MessageData messageData = new MessageData();
        messageData.setAction("delete todo");
        messageData.setKey(String.valueOf(new Date()));
        messageData.setData("success");
        kafkaProducer.sendMessage(messageData);
        return WebResponse.<String>builder().data("Ok").build();
    }
}

package com.faisal.user_api.messaging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaProducer {

    private static final String TOPIC = "my-topic";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessage(Object message) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonValue = mapper.writeValueAsString(message);
            kafkaTemplate.send(TOPIC, jsonValue);
            System.out.println("Message sent");
        } catch (JsonProcessingException e) {
            // Handle JSON processing exception
            throw new RuntimeException(e);
        }
    }
}

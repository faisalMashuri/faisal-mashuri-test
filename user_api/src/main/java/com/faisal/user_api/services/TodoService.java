package com.faisal.user_api.services;

import com.faisal.user_api.entity.Todo;
import com.faisal.user_api.entity.User;
import com.faisal.user_api.model.TodoRequest;
import com.faisal.user_api.repositories.TodoRepository;
import com.faisal.user_api.repositories.UserRepository;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Timestamp;
import java.util.*;

@Service
public class TodoService implements ITodoService{
    @Autowired
    private TodoRepository todoRepository;
    @Autowired
    private UserRepository userRepository;
    private Validator validator;
    @Autowired
    private JWTService jwtService;
    private RedisService redisService;

    public TodoService(TodoRepository todoRepository, Validator validator, JWTService jwtService, RedisService redisService) {
        this.todoRepository = todoRepository;
        this.validator = validator;
        this.jwtService = jwtService;
        this.redisService = redisService;
    }

    @Transactional(rollbackFor = Exception.class)
    public void CreateTodo(TodoRequest request, String token) {
        Set<ConstraintViolation<TodoRequest>> constraintViolations = validator.validate(request);
        if (!constraintViolations.isEmpty()) {
            throw new ConstraintViolationException(constraintViolations);
        }

        String username = jwtService.extractUsername(token);
        User userData = userRepository.findByUsername(username);
        if (userData == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "credential not found");
        }

        if (!jwtService.validateToken(token, userData)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid token");
        }

        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        Todo todo = new Todo();
        todo.setId(UUID.randomUUID().toString());
        todo.setUser(userData);
        todo.setTitle(request.getTitle());
        todo.setDone(request.getIsDone());
        todo.setCreated_at(currentTime);
        todo.setUpdated_at(currentTime);
        todoRepository.save(todo);
        redisService.removeValue(todo.getUser().getId() + "todo");
    }
    @Transactional(rollbackFor = Exception.class)
    public void UpdateTodo(String token, String todo_id, TodoRequest request) {
        Set<ConstraintViolation<TodoRequest>> constraintViolations = validator.validate(request);
        if (!constraintViolations.isEmpty()) {
            throw new ConstraintViolationException(constraintViolations);
        }

        String username = jwtService.extractUsername(token);
        User userData = userRepository.findByUsername(username);
        if (userData == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "credential not found");
        }

        if (!jwtService.validateToken(token, userData)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid token");
        }
        Todo todo = todoRepository.findById(todo_id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Todo not found"));

        if (!Objects.equals(userData.getId(), todo.getUser().getId())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Access denied");
        }
        if (!Objects.equals(request.getTitle(), null)) {
            todo.setTitle(request.getTitle());
        }
        todo.setDone(request.getIsDone());
        todoRepository.save(todo);
        redisService.removeValue(todo.getUser().getId() + "todo");

    }

    @Transactional(rollbackFor = Exception.class)
    public void DeleteTodo(String token, String todo_id) {
        String username = jwtService.extractUsername(token);
        User userData = userRepository.findByUsername(username);
        if (userData == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "credential not found");
        }

        if (!jwtService.validateToken(token, userData)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid token");
        }
        Todo todo = todoRepository.findById(todo_id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Todo not found"));

        if (!Objects.equals(userData.getId(), todo.getUser().getId())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Access denied");
        }
        todoRepository.delete(todo);
        redisService.removeValue(todo.getUser().getId() + "todo");
    }
}

package com.faisal.loggerapi.messaging;

import com.faisal.loggerapi.Message;
import com.faisal.loggerapi.repository.LoggerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class KafkaConsumer {
    @Autowired
    private LoggerRepository loggerRepository;
    @KafkaListener(topics = "my-topic")
    public void listen(String message) {
        System.out.println("Data dari kafka : "+message);
        Message dataMessage = new Message();
        dataMessage.setId(UUID.randomUUID().toString());
        dataMessage.setContent(message);
        loggerRepository.save(dataMessage);
        System.out.println("Data berhasil disimpan");
    }
}
